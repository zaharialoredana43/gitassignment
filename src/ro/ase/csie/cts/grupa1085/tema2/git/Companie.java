package ro.ase.csie.cts.grupa1085.tema2.git;

public class Companie implements InterfataVanzari {
	String sucursala;
	int[] vanzari;
	int numarAngajati;
	int numarOreSpulimentareLunare;

	public Companie(String sucursala, int[] vanzari, int numarAngajati, int numarOreSpulimentareLunare) {
		super();
		this.sucursala = sucursala;
		this.vanzari = vanzari;
		this.numarAngajati = numarAngajati;
		this.numarOreSpulimentareLunare = numarOreSpulimentareLunare;
	}

	@Override
	public int calculeazaNumarulMediuDeVanzari() {
		int numarulVanzarilor = 0;
		for (int unitateVanduta : vanzari) {
			numarulVanzarilor += unitateVanduta;
		}
		return numarulVanzarilor / vanzari.length;
	}

	@Override
	public boolean verificaExistentaVanzarilorNeprofitabile() {
		for (int unitateVanduta : vanzari) {
			if (unitateVanduta < 800) {
				return true;
			}
		}
		return false;
	}

}
