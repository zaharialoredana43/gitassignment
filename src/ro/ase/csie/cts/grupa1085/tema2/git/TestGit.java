package ro.ase.csie.cts.grupa1085.tema2.git;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licenta este: Depozite de date cu aplicatie intr-o societate comerciala");
		Companie alfa = new Companie("Alfa V", new int[] { 100, 600, 900, 7000, 4000 }, 900, 289000);
		System.out.println(alfa.calculeazaNumarulMediuDeVanzari());
		System.out.println(alfa.verificaExistentaVanzarilorNeprofitabile());
	}

}
