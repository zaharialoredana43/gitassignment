package ro.ase.csie.cts.grupa1085.tema2.git;

public interface InterfataVanzari {
public abstract int calculeazaNumarulMediuDeVanzari();
public abstract boolean verificaExistentaVanzarilorNeprofitabile();
}
